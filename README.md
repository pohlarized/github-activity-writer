# About
This tool creates commits that spell out words in the github contribution graph.

# Installation
This tool requires python3.9+ and [poetry](https://python-poetry.org/).

First, clone this repository, then use `poetry install` to install all dependencies.

# Usage
`poetry run python src/main.py [--commits-per-day COMMITS_PER_DAY] "text" repository_path start_date`
This will add new commits to the repository at `repository-path` starting at `start-date` spelling out the text `text`.
Each day will have `COMMITS_PER_DAY` commits.
The text is automatically converted to uppercase.

# How it works
Letters are represented as "pixels" in githubs 4x7 contribution grid.
Each of these pixels corresponds to a date.
Starting from some start date, `COMMITS_PER_DAY` new commits are added for each pixel where the commit date is set to the date corresponding to the current pixel.
For each commit a new file `tmpfile` is added to the root of the repository, filled with a random 16 byte string for each commit, which is also used as the commit message.
