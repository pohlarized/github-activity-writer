#!/usr/bin/env python3
import argparse
import datetime
import subprocess
import os
from pathlib import Path
from typing import NamedTuple


class Args(NamedTuple):
    text: str
    repository_path: Path
    start_date: datetime.date
    commits_per_day: int


class Letter(NamedTuple):
    pixels: list[int]
    column_width: int = 4


LETTERS: dict[str, Letter] = {
    ' ': Letter([]),
    'A': Letter([1, 2, 3, 4, 7, 9, 15, 16, 17, 18]),
    'B': Letter([0, 1, 2, 3, 4, 7, 9, 11, 15, 17]),
    'C': Letter([0, 1, 2, 3, 4, 7, 11, 14, 18]),
    'D': Letter([0, 1, 2, 3, 4, 7, 11, 15, 16, 17]),
    'E': Letter([0, 1, 2, 3, 4, 7, 9, 11, 14, 16, 18]),
    'F': Letter([0, 1, 2, 3, 4, 7, 9, 14, 16]),
    'G': Letter([0, 1, 2, 3, 4, 7, 9, 11, 14, 16, 17, 18]),
    'H': Letter([0, 1, 2, 3, 4, 9, 14, 15, 16, 17, 18]),
    'I': Letter([0, 1, 2, 3, 4], 2),
    'J': Letter([3, 4, 11, 14, 15, 16, 17, 18]),
    'K': Letter([0, 1, 2, 3, 4, 9, 14, 15, 17, 18]),
    'L': Letter([0, 1, 2, 3, 4, 11, 18]),
    'M': Letter([0, 1, 2, 3, 4, 8, 16, 22, 28, 29, 30, 31, 32], 6),
    'N': Letter([0, 1, 2, 3, 4, 8, 9, 16, 17, 21, 22, 23, 24, 25], 5),
    'O': Letter([0, 1, 2, 3, 4, 7, 11, 14, 15, 16, 17, 18]),
    'P': Letter([0, 1, 2, 3, 4, 7, 9, 14, 15, 16]),
    'Q': Letter([0, 1, 2, 3, 4, 7, 10, 11, 14, 15, 16, 17, 18, 26]),
    'R': Letter([0, 1, 2, 3, 4, 7, 9, 14, 15, 17, 18]),
    'S': Letter([0, 1, 2, 4, 7, 9, 11, 14, 16, 17, 18]),
    'T': Letter([0, 7, 8, 9, 10, 11, 14]),
    'U': Letter([0, 1, 2, 3, 4, 11, 14, 15, 16, 17, 18]),
    'V': Letter([0, 1, 2, 3, 11, 14, 15, 16, 17]),
    'W': Letter([0, 1, 2, 3, 4, 10, 16, 24, 28, 29, 30, 31, 32], 6),
    'X': Letter([0, 4, 8, 9, 10, 15, 16, 17, 21, 25], 5),
    'Y': Letter([0, 1, 2, 9, 10, 11, 21, 22, 23]),
    'Z': Letter([0, 3, 4, 7, 9, 11, 14, 15, 18]),
}


def get_next_monday(start_date: datetime.date) -> datetime.date:
    weekday = start_date.weekday()
    days_until_monday = (0 - weekday) % 7
    return start_date + datetime.timedelta(days=days_until_monday)


def commit_dates_for_letter(
    letter: str, initial_commit_date: datetime.date
) -> list[datetime.date]:
    commit_dates = [
        initial_commit_date + datetime.timedelta(days=offset) for offset in LETTERS[letter].pixels
    ]
    return commit_dates


def commit_dates_for_text(text: str, initial_commit_date: datetime.date) -> list[datetime.date]:
    date_ptr = initial_commit_date
    commit_dates = []
    for letter in text.upper():
        commit_dates += commit_dates_for_letter(letter, date_ptr)
        date_ptr = date_ptr + datetime.timedelta(days=7 * LETTERS[letter].column_width)
    return commit_dates


def add_commit(repo_path: Path, date: datetime.date) -> None:
    commit_msg = os.urandom(16).hex()
    env = os.environ
    env['GIT_COMMITTER_DATE'] = date.isoformat()
    subprocess.run(
        ['git', 'commit', '--allow-empty', '--date', date.isoformat(), '--message', commit_msg],
        check=True,
        cwd=repo_path,
        capture_output=True,
        timeout=10,
        env=env,
    )
    print(f'Successfully added commit for {date.isoformat()}')


def add_commits(repo_path: Path, commit_dates: list[datetime.date], repetitions: int) -> None:
    for date in commit_dates:
        for _ in range(repetitions):
            add_commit(repo_path, date)


def parse_args() -> Args:
    parser = argparse.ArgumentParser(description='Write words to your github activity graph.')
    parser.add_argument('text', help='Text to write to the repository', type=str)
    parser.add_argument(
        'repository_path', help='Path to the repository to add the commits to', type=Path
    )
    parser.add_argument(
        'start_date',
        help='Date of the first commit in isoformat YYYY-MM-DD',
        type=datetime.datetime.fromisoformat,
    )
    parser.add_argument(
        '--commits-per-day',
        '-c',
        help=(
            'How often to repeat each commit. '
            'This should be larger than the highest number of commits per day '
            'you expect to naturally get on your profile.'
        ),
        type=int,
        default=10,
    )
    args: Args = parser.parse_args(namespace=Args)  # type: ignore
    return args


def main() -> None:
    args = parse_args()
    first_commit_date = get_next_monday(args.start_date)
    commit_dates = commit_dates_for_text(args.text, first_commit_date)
    add_commits(args.repository_path, commit_dates, args.commits_per_day)


if __name__ == '__main__':
    main()
